package com.ruoyi.erp.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.erp.domain.ErpAttributeItem;
import com.ruoyi.erp.service.IErpAttributeItemService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 拓展属性值Controller
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Controller
@RequestMapping("/erp/erpAttributeItem")
public class ErpAttributeItemController extends BaseController
{
    private String prefix = "erp/erpAttributeItem";

    @Autowired
    private IErpAttributeItemService erpAttributeItemService;

    @RequiresPermissions("erp:erpAttributeItem:view")
    @GetMapping()
    public String erpAttributeItem()
    {
        return prefix + "/erpAttributeItem";
    }

    /**
     * 查询拓展属性值列表
     */
    @RequiresPermissions("erp:erpAttributeItem:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ErpAttributeItem erpAttributeItem)
    {
        startPage();
        List<ErpAttributeItem> list = erpAttributeItemService.selectErpAttributeItemList(erpAttributeItem);
        return getDataTable(list);
    }

    /**
     * 导出拓展属性值列表
     */
    @RequiresPermissions("erp:erpAttributeItem:export")
    @Log(title = "拓展属性值", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ErpAttributeItem erpAttributeItem)
    {
        List<ErpAttributeItem> list = erpAttributeItemService.selectErpAttributeItemList(erpAttributeItem);
        ExcelUtil<ErpAttributeItem> util = new ExcelUtil<ErpAttributeItem>(ErpAttributeItem.class);
        return util.exportExcel(list, "erpAttributeItem");
    }

    /**
     * 新增拓展属性值
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存拓展属性值
     */
    @RequiresPermissions("erp:erpAttributeItem:add")
    @Log(title = "拓展属性值", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ErpAttributeItem erpAttributeItem)
    {
        return toAjax(erpAttributeItemService.insertErpAttributeItem(erpAttributeItem));
    }

    /**
     * 修改拓展属性值
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        ErpAttributeItem erpAttributeItem = erpAttributeItemService.selectErpAttributeItemById(id);
        mmap.put("erpAttributeItem", erpAttributeItem);
        return prefix + "/edit";
    }

    /**
     * 修改保存拓展属性值
     */
    @RequiresPermissions("erp:erpAttributeItem:edit")
    @Log(title = "拓展属性值", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ErpAttributeItem erpAttributeItem)
    {
        return toAjax(erpAttributeItemService.updateErpAttributeItem(erpAttributeItem));
    }

    /**
     * 删除拓展属性值
     */
    @RequiresPermissions("erp:erpAttributeItem:remove")
    @Log(title = "拓展属性值", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(erpAttributeItemService.deleteErpAttributeItemByIds(ids));
    }
}
